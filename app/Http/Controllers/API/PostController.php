<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use Validator;
use Auth;

class PostController extends BaseController
{

    public function guard()
    {
        return Auth::guard();
    }

    //show all post
    public function index(){

        $posts = Post::all();
        return $this->sendResponse(PostResource::collection($posts), 'Post retrieved successfully.');
    }

    //create post
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
            'content' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $posts = Post::create($input);
   
        return $this->sendResponse(new PostResource($posts), 'Post created successfully.');
    } 
    //show detail by id
    public function show($id)
    {
        $posts = Post::find($id);
  
        if (is_null($posts)) {
            return $this->sendError('Post not found.');
        }
   
        return $this->sendResponse(new PostResource($posts), 'Post retrieved successfully.');
    }
    //update post
    public function update(Request $request, Post $post)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'title' => 'required',
            'content' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $post->title = $input['title'];
        $post->content = $input['content'];
        $post->save();
   
        return $this->sendResponse(new PostResource($post), 'Post updated successfully.');
    }

    //delete post

    public function destroy(Post $post)
    {
        $post->delete();
   
        return $this->sendResponse([], 'Post deleted successfully.');
    }
}
